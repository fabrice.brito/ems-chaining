$graph:
- class: Workflow
  id: burned-area
  doc: Burned area detection based on NDVI/NDWI thresholds
  label: Burned area
  inputs:
    start_date:
      doc: Time of interest start date
      label: Time of interest start date
      type: string
    stop_date:
      doc: Time of interest stop date
      label: Time of interest stop date
      type: string
    aoi:
      doc: Area of interest
      label: Area of interest
      type: string
    ndvi_threshold:
      doc: NDVI threshold
      label: NDVI threshold
      type: string?
    ndwi_threshold:
      doc: NDWI threshold
      label: NDWI threshold
      type: string?
  
  outputs:
    - id: wf_outputs
      outputSource:
        - step_2/results
      type: FeatureCollection
  
  steps:
    step_1:
      in:
        start_date: start_date
        stop_date: stop_date
        aoi: aoi      
      out:
        - features_results
      run: '#query'
    
    step_2:
      in:
        features:
          source: step_1/features_results
        ndvi_threshold: ndvi_threshold
        ndwi_threshold: ndwi_threshold
      out:
        - results
      run: '#process'

- class: FeatureTool
  id: query
  doc: queries an OGC API Features endpoint
  inputs:
    start_date:
      type: string
    stop_date:
      type: string
    aoi: 
      type: string
  outputs:
    features_results:
      type: FeatureCollection
  requirements:
    CollectionRequirement:
      collectionUri: https://catalog.terradue.com/sentinel2
  
- class: ProcessTool
  id: process
  doc: submits an execution request to an OGC API Processes endpoint
  inputs:
    features:
      type: FeatureCollection
    ndvi_threshold:
      type: string?
    ndwi_threshold:
      type: string?
  outputs:
    results:
      type: FeatureCollection
  requirements:
    ApplicationPackageRequirement:
      applicationPackageUri: https://raw.githubusercontent.com/EOEPCA/app-snuggs/main/app-package.cwl
    ExecutionRequirement:
      adesUri: https://platorm-a.somedomain.org/process

$namespaces:
  s: https://schema.org/
s:softwareVersion: 1.0
cwlVersion: v1.0
schemas:
  - http://schema.org/version/9.0/schemaorg-current-http.rdf

