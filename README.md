# ems-chaining


## class FeatureTool 

The `FeatureTool` class invokes an OGC API Features endpoint.

It takes as inputs the queryables values.

It generates a `FeatureCollection` object.

#### CollectionRequirement

| field                     | required 	| type 	    | description 	                        |
|---                        |---	    |---	    |---                                    |
| `class`                   | required 	| string   	| Always `CollectionRequirement`        |
| `collectionUri`           | required 	| string    | EO Collection URI                     |


#### Queryable

| field                     | required 	| type 	                        | description 	                                                            |
|---                        |---	    |---	                        |---                                                                        |
| `class`                   | required 	| constant value `Queryable`   	| Must be `Queryable` to indicate this object describes a `Queryable`.      |
| `mapping`                 | required 	| string                        | queryable **thing**                                                       |

Example:

```yaml
inputs:
    start_date:
        type: Queryable
        mapping: "time:start"
    stop_date:
        type: Queryable
        mapping: "time:end"
    aoi: 
        type: Queryable
        mapping: "geo:box"
```


## class ProcessTool 

The `ProcessTool` class invokes an OGC API Processes endpoint.

It takes processing inputs that may include a `FeatureCollection`. 

It generates a `FeatureCollection` object.

#### ApplicationPackageRequirement

Fields

| field                     | required 	| type 	    | description 	                        |
|---                        |---	    |---	    |---                                    |
| `class`                   | required 	| string   	| Always `ApplicationPackageRequirement`|
| `applicationPackageUri`   | required 	| string    | URL to the Application Package        |


#### ExecutionRequirement

| field                     | required 	| type 	    | description 	                        |
|---                        |---	    |---	    |---                                    |
| `class`                   | required 	| string   	| Always `ExecutionRequirement`         |
| `adesUri `                | required 	| string    | URL to the OGC API Processes endpoint |
     

## Examples

### Example 1 

Given and area of interest, a time of interest and the NDVI/NDWI thresholds, the Workflow runs two steps. 
The first executes a catalog query on the Sentinel-2 collection over the area of interest and covering the time of interest and produces a feature collection.
The second executes a process execution request using an application package. The selection of the ADES is defined at runtime. This step produces a feature collection.

### Example 2

Given and area of interest, a time of interest and the NDVI/NDWI thresholds, the Workflow runs two steps. 
The first executes a catalog query on the Sentinel-2 collection over the area of interest and covering the time of interest and produces a feature collection.
The second executes a process execution request using an application package on a defined ADES.
